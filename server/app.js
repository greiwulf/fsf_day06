//load library express
var express = require("express");
//load library path
var path = require("path");

//create an instance of express application
var app = express();
  
// serve files from public directory
// __dirname is the absolute path of the application directory
app.use(express.static(path.join(__dirname, "/../public/pages")));
app.use(express.static(path.join(__dirname, "/../public/js")));
//mask bower_components into lib directory
app.use("/lib", express.static(path.join(__dirname, "/../server/bower_components")));
    
//define the port that our app is going to listen to
app.set("port", parseInt(process.argv[2]) || 3000);

//start the server
app.listen(app.get("port"), function() {
  console.log("Application started at %s on %d", new Date(), app.get("port"));
});
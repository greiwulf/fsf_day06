// IIFE
(function(){
  // create angular module
  var modApp = angular.module("modApp", []);

    // controller functions
    var FirstCtrl = function() {

      // fix reference for this
      var firstCtrl = this;

      // models
      firstCtrl.item = "";
      firstCtrl.qty = 0;
      firstCtrl.basket = [];
      firstCtrl.filterText;

      firstCtrl.updateBasket = function() {
        // add item
        var idx = firstCtrl.basket.findIndex(function(elem){
          return(elem.item == firstCtrl.item)
        })

        if (idx == -1) {
        firstCtrl.basket.push({
          item: firstCtrl.item,
          qty: firstCtrl.qty
        });
        } else {
          firstCtrl.basket[idx].qty += firstCtrl.qty;
        }


        // reset
        firstCtrl.item="";
        firstCtrl.qty=0;        
      };
 
    }

//define a controller called FirstCtrl
modApp.controller("FirstCtrl", [ FirstCtrl ]);

})();

